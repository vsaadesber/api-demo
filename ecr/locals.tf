locals {
  env = {
  demo-local = {
    aws_profile                = "default"
    aws_region                 = "ap-southeast-2"
    principals_full_access     = []
    principals_readonly_access = []
    tags = {
      Name       = var.container_name
      repository = "api-demo/ecr"
      workspace  = terraform.workspace

    }
  }

    demo-shared = {
      aws_profile                = "demo-shared"
      aws_region                 = "ap-southeast-2"
      principals_full_access     = []
      principals_readonly_access = ["354334841216"]
      tags = {
        Name       = var.container_name
        repository = "api-demo/ecr"
        workspace  = terraform.workspace
      }
    }
  }


  workspace = local.env[terraform.workspace]
}