data "aws_kms_key" "key" {
  key_id = lookup(local.workspace, "kms_key_alias", "alias/aws/ebs")
}