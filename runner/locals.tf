locals {
  env = {
    demo-shared = {
      aws_profile = "demo-shared"
      aws_region  = "ap-southeast-2"
      # account_id                          = 722141136946
      vpc_name                            = "guacamole-vpc"
      subnet_tier                         = "*private*"
      create_gitlab_runner                = true
      create_gitlab_runner_primary_role   = true
      create_gitlab_runner_secondary_role = true
      instance_type                       = "t3.micro"
      instance_ssm_key_path               = "/gitlab/api-demo/token"
      volume_size                         = 30
      timezone                            = "Australia/Sydney"
      iam_instance_profile                = "gitlab-runner-primary-role"
      kms_key_alias                       = "alias/aws/ebs"
      keypair_name                        = "fr-aws"

      autoscaling_group = {
        max_size         = 1
        min_size         = 1
        desired_capacity = 1
      }
      gitlab = {
        https_url       = "https://gitlab.com/"
        ssh_url         = "gitlab.com"
        concurrent      = "2"
        executor        = "shell"
        runner_name     = "gl-runner-test"
        runner_tags     = "3M-shell"
        token_ssm_param = "/gitlab/api-demo/token"
      }
      # Scheduling
      enable_autoscaling_schedule = false
      autoscaling_schedule_start  = "00 21 * * MON-FRI" # GMT +10 Startup instances each weekday at 07:00 (UTC to Australia/Sydney)
      autoscaling_schedule_stop   = "30 09 * * MON-FRI" # GMT +10 Stop instances each weekday at 19:30 (UTC to Australia/Sydney)
      tags = {
        repository = "api-demo/runners"
        workspace  = terraform.workspace
      }
    }


    demo-dev = {
      aws_profile = "demo-dev"
      aws_region  = "ap-southeast-2"
      # account_id                          = 722141136946
      # account_id                          = 354334841216
      create_gitlab_runner                = false
      create_gitlab_runner_primary_role   = false
      create_gitlab_runner_secondary_role = true
      tags = {
        repository = "api-demo/runners"
        workspace  = terraform.workspace
      }
    }

  }

  account_id = 722141136946

  workspace = local.env[terraform.workspace]
}
