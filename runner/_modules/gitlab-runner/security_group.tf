resource "aws_security_group" "gitlab_runner" {
  name        = lower(var.gitlab.runner_name)
  description = "Security Group for Gitlab Runners"
  vpc_id      = data.aws_vpc.main.id

  # Ingress
  dynamic "ingress" {
    for_each = var.sg_ingress_ports
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      description = "Terraform Managed"
      cidr_blocks = var.whitelist_cidr_block
    }
  }

  # Egress - Allow all traffic
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    var.tags,
    { Name = lower(var.gitlab.runner_name) }
  )

}
