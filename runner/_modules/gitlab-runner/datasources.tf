data "aws_vpc" "main" {
  tags = {
    Name = var.vpc_name
  }
}

data "aws_subnet_ids" "application" {
  vpc_id = data.aws_vpc.main.id

  tags = {
    Name = var.subnet_tier
  }
}

data "aws_subnet" "tier_a" {
  vpc_id = data.aws_vpc.main.id

  tags = {
    Name = var.subnet_tier
  }

  filter {
    name   = "availability-zone"
    values = ["${var.aws_region}a"]
  }
}

data "aws_subnet" "tier_b" {
  vpc_id = data.aws_vpc.main.id

  tags = {
    Name = var.subnet_tier
  }

  filter {
    name   = "availability-zone"
    values = ["${var.aws_region}b"]
  }
}

data "aws_subnet" "tier_c" {
  vpc_id = data.aws_vpc.main.id

  tags = {
    Name = var.subnet_tier
  }

  filter {
    name   = "availability-zone"
    values = ["${var.aws_region}c"]
  }
}

data "aws_ami" "amazon_linux_2" {
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-ebs"]
  }

  most_recent = true
  owners      = ["amazon"]
}

data "aws_iam_instance_profile" "runner" {
  name = var.iam_instance_profile
}