
resource "aws_iam_role" "gitlab_runner_secondary" {
  count                = var.create_gitlab_runner_secondary_role == true ? 1 : 0
  name                 = "gitlab-runner-secondary-role"
  description          = "Used by gitlab infra runners to coordinate and trigger terraform actions in other accounts"
  assume_role_policy   = data.aws_iam_policy_document.gitlab_runner_secondary.json
}
data "aws_iam_policy_document" "gitlab_runner_secondary" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${var.account_id}:role/gitlab-runner-primary-role"]
    }
  }
}

resource "aws_iam_policy" "gitlab_runner_secondary" {
  count      = var.create_gitlab_runner_secondary_role == true ? 1 : 0
  name       = "gitlab-runner-secondary-policy"
  description = "Used by gitlab infra runners to coordinate and trigger terraform actions in other accounts"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "*",
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_instance_profile" "gitlab_runner_secondary" {
  count      = var.create_gitlab_runner_secondary_role == true ? 1 : 0
  name       = "gitlab-runner-secondary-role"
  role       = aws_iam_role.gitlab_runner_secondary[0].name
}

resource "aws_iam_role_policy_attachment" "gitlab_runner_secondary" {
  count      = var.create_gitlab_runner_secondary_role == true ? 1 : 0
  role       = aws_iam_role.gitlab_runner_secondary[0].name
  policy_arn = aws_iam_policy.gitlab_runner_secondary[0].arn
}

