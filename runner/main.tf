module "iam" {
  source                              = "./_modules/gitlab-iam"
  account_id                          = local.account_id
  create_gitlab_runner_primary_role   = local.workspace.create_gitlab_runner_primary_role
  create_gitlab_runner_secondary_role = local.workspace.create_gitlab_runner_secondary_role

}


module "runner" {
  count                 = local.workspace.create_gitlab_runner ? 1 : 0
  depends_on            = [module.iam]
  source                = "./_modules/gitlab-runner"
  aws_region            = lookup(local.workspace, "aws_region", null)
  vpc_name              = lookup(local.workspace, "vpc_name", null)
  subnet_tier           = lookup(local.workspace, "subnet_tier", null)
  instance_type         = lookup(local.workspace, "instance_type", null)
  volume_size           = lookup(local.workspace, "volume_size", null)
  sg_ingress_ports      = lookup(local.workspace, "security_group_ingress_ports", [])
  whitelist_cidr_block  = lookup(local.workspace, "whitelist_cidr_block", null)
  timezone              = lookup(local.workspace, "timezone", null)
  instance_ssm_key_path = lookup(local.workspace, "instance_ssm_key_path", null)
  autoscaling_group     = lookup(local.workspace, "autoscaling_group", null)
  gitlab                = lookup(local.workspace, "gitlab", null)
  iam_instance_profile  = lookup(local.workspace, "iam_instance_profile", null)
  keypair_name          = lookup(local.workspace, "keypair_name", null)
  kms_key_id            = data.aws_kms_key.key.arn
  tags                  = lookup(local.workspace, "tags", null)
  # Autoscaling schedule configuration
  enable_autoscaling_schedule = lookup(local.workspace, "enable_autoscaling_schedule", true)
  autoscaling_schedule_start  = lookup(local.workspace, "autoscaling_schedule_start", null)
  autoscaling_schedule_stop   = lookup(local.workspace, "autoscaling_schedule_stop", null)

}
