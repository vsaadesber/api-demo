resource "aws_cloudwatch_log_group" "main" {
  name              = local.workspace.name
  retention_in_days = local.workspace.log_retention_days
  tags              = local.workspace.tags
}